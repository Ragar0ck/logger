package net.ragarock.core.util;

//this code is property of Jolan Vermeulen aka Ragarock on the Internet.
//License blablabla...
//i am not responsible if this code kills your family, pets, rocks or your computer.

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;

public class Logger {

    //this does not automagically print to System.out you have add this as well
    //(Logger.outputs.add(System.out))
    public static ArrayList<PrintStream> outputs = new ArrayList<>();
    //set the DEBUG_MODE to true if you want to see the debug messages
    //else they will be ignored
    public static boolean DEBUG_MODE;

    public static void error(Throwable e) {
        for(PrintStream out: outputs) {
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            out.println("[ERROR] " + "[" + new Date() + "] " + "[" + elements[2].getClassName() + "] " + e.getMessage());

            e.printStackTrace(out);
        }
    }

    public static void error(String message) {
        for(PrintStream out: outputs) {
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            out.println("[ERROR] " + "[" + new Date() + "] " + "[" + elements[2].getClassName() + "] " + message);
        }
    }
    public static void error(String message, String error) {
        for(PrintStream out: outputs) {
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            out.println("[ERROR] " + "[" + new Date() + "] " + "[" + elements[3].getClassName() + "] " + message);
            out.println("[ERROR] " + "[" + new Date() + "] " + "[" + elements[3].getClassName() + "] " + error);
        }
    }

    public static void error(String message, Throwable e) {
        error(message, e.getMessage());
    }

    public static void warn(String message) {
        for(PrintStream out: outputs) {
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            out.println("[WARNING] " + "[" + new Date() + "] " + "[" + elements[2].getClassName() + "] " + message);
        }
    }

    public static void warn(String message, String error) {
        for(PrintStream out: outputs) {
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            out.println("[WARNING] " + "[" + new Date() + "] " + "[" + elements[3].getClassName() + "] " + message);
            out.println("[WARNING] " + "[" + new Date() + "] " + "[" + elements[3].getClassName() + "] " + error);
        }
    }

    public static void warn(Throwable e) {
        for(PrintStream out: outputs) {
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            out.println("[WARNING] " + "[" + new Date() + "] " + "[" + elements[2].getClassName() + "] " + e.getMessage());

            e.printStackTrace(out);
        }
    }

    public static void warn(String message, Throwable e) {
        warn(message, e.getMessage());
    }

    public static void info(String message) {
        for(PrintStream out: outputs) {
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            out.println("[INFO] " + "[" + new Date() + "] " + "[" + elements[2].getClassName() + "] " + message);
        }
    }

    public static void debug(String message) {
        if(DEBUG_MODE) {
            for(PrintStream out: outputs) {
                StackTraceElement[] elements = Thread.currentThread().getStackTrace();
                out.println("[DEBUG] " + "[" + new Date() + "] " + "[" + elements[2].getClassName() + "] " + message);
            }
        }
    }

}